{\rtf1}

<HTML> 
<BODY> 
<H1>Logo Awesome Art</H1> 
<img src="https://firebasestorage.googleapis.com/v0/b/myapp-ae4a9.appspot.com/o/Logo%20terminado.png?alt=media&token=0a0d545c-9c48-4a03-bf45-5a2f73f9f07a"alt="Logo Awesome Art" width="90" height="90" border="4">
</BODY> 
</HTML> 

<HTML> 
<BODY> 
<H1>Apariencia Final</H1>
<H1>Inicio & Menu</H1> 
<img src="https://firebasestorage.googleapis.com/v0/b/myapp-ae4a9.appspot.com/o/5.jpeg?alt=media&token=4ccbaef3-2e15-4afd-b244-d82b8e239192"alt="Inicio" width="90" height="90" border="4">
<img src="https://firebasestorage.googleapis.com/v0/b/myapp-ae4a9.appspot.com/o/6.jpeg?alt=media&token=3e3bf9b8-dffd-48cd-abc2-0fe90d4f3128"alt="Menu" width="90" height="90" border="4">
<img src="https://firebasestorage.googleapis.com/v0/b/myapp-ae4a9.appspot.com/o/7.jpeg?alt=media&token=2dad14fc-6cb1-4b16-880c-4f1a5436c5d6"alt="Menu" width="90" height="90" border="4">
<H1>Curiosidades</H1>
<img src="https://firebasestorage.googleapis.com/v0/b/myapp-ae4a9.appspot.com/o/8.jpeg?alt=media&token=c1305aaf-7ed5-4453-b8d4-b4c42a8a4ead"alt="Logo Awesome Art" width="90" height="90" border="4">
<img src="https://firebasestorage.googleapis.com/v0/b/myapp-ae4a9.appspot.com/o/9.jpeg?alt=media&token=c6fbacb8-ac0b-4672-9bc7-b256061a2d52"alt="Logo Awesome Art" width="90" height="90" border="4">
</BODY> 
</HTML>







Nombre de la app: Awesome Art

Api de donde extraigo la informacion: http://www.vam.ac.uk/api/json/museumobject/

Link de la pagina web la cual cargo en una pestaña del menu:  https://www.vam.ac.uk/




Funcionalidad:
La aplicación es sobre obras de arte.
La aplicación empieza cargando un home obtenido apartir de la api en el cual podemos ver detalles de la obra y añadirlos a favoritos, también podemos en el menu desplegable ir
a configuración y en ajustes modificar el año y poder ver obras mas antiguas por ejemplo.
En el menu desplazable que tenemos podemos encontrar varias opciones desde un perfil que esta en desarrollo y se puede ver como quedara el resultado aproximado, tambien tenemos en 
configuracion unos ajustes donde podemos modificar el año para que nos muestre cosas mas antiguas a ese año.
Otras opciones que podemos encontrar es ir a visitar la pagina web del museo Victoria & Albert donde podemos ver infomación y novedades del museo.
Por si nos perdemos y queremos ir a visitar el museo en el apartado de Google maps podras encontrar geolocalizado el centro de Londres y la ubicación del museo.
Para los mas curiosos la aplicación ofrece una opcion donde podemos buscar mas obras introduciendo una palabra, autor y nos mostrara cosas sobre ello.
Por último tenemos una pestaña donde nos muestra nuestras obras favoritas.
La aplicación dispone de Fire Base que simplemente deja añadir elementos y borrarlos (no tiene nada que ver con la app, en futuro puede que si)

Al final del menu tenemos informacion sobre la aplicación y el idioma que nos permite cambiar entre español y ingles la aplicación.
