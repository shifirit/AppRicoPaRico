{\rtf1}

<HTML> 
<BODY> 
<H1>Logo Awesome Art</H1> 
<img src="https://firebasestorage.googleapis.com/v0/b/myapp-ae4a9.appspot.com/o/Logo%20terminado.png?alt=media&token=0a0d545c-9c48-4a03-bf45-5a2f73f9f07a"alt="Logo Awesome Art" width="90" height="90" border="4">
</BODY> 
</HTML> 

<HTML> 
<BODY> 
<H1>Final Appearance</H1>
<H1>Home & Menu</H1>
<img src="https://firebasestorage.googleapis.com/v0/b/myapp-ae4a9.appspot.com/o/1.jpeg?alt=media&token=a7917330-c96c-4562-821e-7ce8c8469149"alt="Home Art" width="90" height="90" border="4">
<img src="https://firebasestorage.googleapis.com/v0/b/myapp-ae4a9.appspot.com/o/2.jpeg?alt=media&token=e0c9166f-c380-435b-b68c-71f7870dfec4"alt="Menu" width="90" height="90" border="4">
<img src="https://firebasestorage.googleapis.com/v0/b/myapp-ae4a9.appspot.com/o/3.jpeg?alt=media&token=59916ac0-a0b1-4029-a2f6-f07445b1ba1d"alt="Menu" width="90" height="90" border="4">
<H1>Curiosities</H1>
<img src="https://firebasestorage.googleapis.com/v0/b/myapp-ae4a9.appspot.com/o/4.jpeg?alt=media&token=8bcd983f-e0e5-4b05-a77b-a95c9e541292"alt="Favourites" width="90" height="90" border="4">
<img src="https://firebasestorage.googleapis.com/v0/b/myapp-ae4a9.appspot.com/o/8.jpeg?alt=media&token=c1305aaf-7ed5-4453-b8d4-b4c42a8a4ead"alt="Search" width="90" height="90" border="4">
</BODY> 
</HTML>



Name of the app: Awesome Art

Api where I extract the information: http://www.vam.ac.uk/api/json/museumobject/

Link of the web page which I charge in a tab of the menu: https://www.vam.ac.uk/



Functionality:
The application is about works of art.
The application starts loading a home obtained from the api in which we can see details of the work and add them to favorites, we can also in the drop down menu go
To settings and in adjustments to modify the year and to be able to see older works for example.
In the scrollable menu that we have we can find several options from a profile that is under development and you can see how the approximate result will be, we also have in
Configuration settings where we can modify the year to show us things older to that year.
Other options that we can find is to visit the website of the Victoria & Albert Museum where we can see information and news of the museum.
In case we lose and we want to go to visit the museum in the section of Google maps you can find geologised the center of London and the location of the museum.
For the most curious the application offers an option where we can search for more works by entering a word, author and show us things about it.
Finally we have a tab where shows our favorite works.
The application has Fire Base that simply lets you add items and delete them (it has nothing to do with the app, in the future you can)

At the end of the menu we have information about the application and the language that allows us to switch between Spanish and English application.
