package com.herprogramacion.restaurantericoparico.model;

import android.text.TextUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.Serializable;
import java.util.ArrayList;

public class Book implements Serializable {
    private int id;
    private String openLibraryId;
    private String author;
    private String title;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getOpenLibraryId() {
        return openLibraryId;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }


    // Get image from API
    public String getCoverUrl() {
        if(openLibraryId.length()<6)
            return"";
        return "http://media.vam.ac.uk/media/thira/collection_images/" +
                openLibraryId.substring(0,6) + "/" +
                openLibraryId + "_jpg_s.jpg";
    }

    // Get image from API
    public String getLargeCoverUrl() {
        if(openLibraryId.length()<6)
            return"";
        return "http://media.vam.ac.uk/media/thira/collection_images/" +
                openLibraryId.substring(0,6) + "/" +
                openLibraryId + ".jpg";
    }


        // Returns a Book given the expected JSON
    public static Book fromJson(JSONObject jsonObject) {
        Book book = new Book();
        try {
            // Deserialize json into object fields
            // Check if a cover edition is available
            JSONObject fields = jsonObject.getJSONObject("fields");
            book.id = jsonObject.getInt("pk");
            book.title = fields.has("title") ? fields.getString("title") : "";
            book.author = fields.has("artist") ? fields.getString("artist") : "";
            book.openLibraryId = fields.has("primary_image_id") ? fields.getString("primary_image_id") : "";
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return book;
    }


    // Decodes array of book json results into business model objects
    public static ArrayList<Book> fromJson(JSONArray jsonArray) {
        ArrayList<Book> books = new ArrayList<Book>(jsonArray.length());
        // Process each result in json array, decode and convert to business object
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject bookJson = null;
            try {
                bookJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
            Book book = Book.fromJson(bookJson);
            if (book != null) {
                books.add(book);
            }
        }
        return books;
    }

    public JSONObject toJSON(){
        JSONObject book = new JSONObject();
        try {
            book.put("pk", id);
            JSONObject fields = new JSONObject();
            fields.put("artist",author);
            fields.put("title",title);
            fields.put("primary_image_id",openLibraryId);


            book.put("fields",fields);
        }catch(Exception e){}
        return book;
    }
}