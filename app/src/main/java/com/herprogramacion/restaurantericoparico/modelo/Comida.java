package com.herprogramacion.restaurantericoparico.modelo;

/**
 * Modelo de datos estático para alimentar la aplicación
 */
public class Comida {
    private float precio;
    private String nombre;
    private int idDrawable;
    private String descripcion;
    private float rating;
    private int position;

    public Comida(float precio, String nombre, int idDrawable , String descripcion , float rating , int position) {
        this.precio = precio;
        this.nombre = nombre;
        this.idDrawable = idDrawable;
        this.descripcion = descripcion;
        this.rating = rating;
        this.position = position;

    }
    public float getPrecio() {
        return precio;
    }

    public String getNombre() {
        return nombre;
    }

    public int getIdDrawable() {
        return idDrawable;
    }

    public String getDescripcion(){
        return  descripcion;
    }

    public float getRating() {
        return rating;
    }

    public int getPosition(){return position;}
}
