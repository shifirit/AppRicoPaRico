package com.herprogramacion.restaurantericoparico.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.herprogramacion.restaurantericoparico.R;
import com.herprogramacion.restaurantericoparico.database.database1.DBAdapter;
import com.herprogramacion.restaurantericoparico.model.Book;
import com.herprogramacion.restaurantericoparico.net.BookClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Fragmento para la sección de "Inicio"
 */
public class FragmentoPreferidos extends Fragment {
    private RecyclerView reciclador;
    private LinearLayoutManager layoutManager;
    private AdaptadorInicio adaptador;

    private BookClient client;

    public FragmentoPreferidos() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_fav, container, false);

        reciclador = (RecyclerView) view.findViewById(R.id.reciclador);
        layoutManager = new LinearLayoutManager(getActivity());
        reciclador.setLayoutManager(layoutManager);

        adaptador = new AdaptadorInicio(getContext());
        reciclador.setAdapter(adaptador);
        DBAdapter dbadapter= new DBAdapter(this.getContext());
        dbadapter.open();

        for(Book libro:dbadapter.obtenerFavoritos()){
            adaptador.add(libro);
        }
        return view;
    }

}
