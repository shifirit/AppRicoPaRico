package com.herprogramacion.restaurantericoparico.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import com.herprogramacion.restaurantericoparico.R;


/**
 * Created by usuario on 28/04/2017.
 */
public class SplashActivity extends Activity {
    private final int ANIMATION_DURATION = 3000;
    private RelativeLayout relativeLayout;
    private Animation animation;

    @Override
    protected void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        setContentView(R.layout.splash);

        relativeLayout = (RelativeLayout) findViewById(R.id.main_layout);
        animation = AnimationUtils.loadAnimation(this, R.anim.up_translation);
        animation.setDuration(400);

        relativeLayout.startAnimation(animation);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, ActividadPrincipal.class));
                SplashActivity.this.finish();
            }
        }, ANIMATION_DURATION);
    }

    @Override
    protected void onResume(){
        super.onResume();
    }
}
