package com.herprogramacion.restaurantericoparico.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.herprogramacion.restaurantericoparico.R;
import com.herprogramacion.restaurantericoparico.database.database1.DBAdapter;
import com.herprogramacion.restaurantericoparico.model.Book;
import com.squareup.picasso.Picasso;

import java.util.List;


public class DetailActivity  extends AppCompatActivity {
    private static final String EXTRA_POSITION = "com.herprogramacion.cursospoint.extra.POSITION";
    private static int position;
    private static List<Book> itemss;
    private boolean esFavorito;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
       // Toast alert = Toast.makeText(this,"Valor Position"+position,Toast.LENGTH_LONG);
        //alert.show();


        setToolbar(); // Reemplazar la action bar

        // Se obtiene la posición del item seleccionado
        int position = getIntent().getIntExtra(EXTRA_POSITION, -1);
        // Carga los datos en la vista
        setupViews(itemss,position);
    }
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)// Habilitar Up Button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    private void setupViews(List<Book> items,int position) {
        this.position = position;
        TextView name = (TextView) findViewById(R.id.detail_name);
        TextView description = (TextView) findViewById(R.id.detail_description);
        TextView author = (TextView) findViewById(R.id.detail_author);
        //TextView price = (TextView) findViewById(R.id.detail_price);
        //RatingBar rating = (RatingBar) findViewById(R.id.detail_rating);
        ImageView image = (ImageView) findViewById(R.id.detail_image);
        final ImageButton favorito = (ImageButton) findViewById(R.id.sharepreference);
        final DBAdapter dbadapter= new DBAdapter(this);
        dbadapter.open();

        // Obtiene el curso ha detallar basado en la posición
        final Book libro = items.get(position);
        name.setText(libro.getTitle());
        description.setText(libro.getTitle());
        author.setText("Creado Por:" + libro.getAuthor());
        //price.setText("$" + libro.getPrecio());
        //rating.setRating(libro.getRating());
        Picasso.with(this).load(Uri.parse(libro.getLargeCoverUrl())).error(R.drawable.cafe).into(image);
        esFavorito = dbadapter.esFavorito(libro);
        favorito.setImageResource(esFavorito?R.drawable.ic_wish_full:R.drawable.ic_menu_star_full);
        favorito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!esFavorito)
                    dbadapter.añadirFavorito(libro);
                else
                    dbadapter.eliminarFavorito(libro);
                esFavorito = !esFavorito;
                favorito.setImageResource(esFavorito?R.drawable.ic_wish_full:R.drawable.ic_menu_star_full);
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
            case android.R.id.home:
                // Obtener intent de la actividad padre
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                // Comprobar si DetailActivity no se creó desde CourseActivity
                if (NavUtils.shouldUpRecreateTask(this, upIntent) || this.isTaskRoot()) {
                    // Construir de nuevo la tarea para ligar ambas actividades
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    // Terminar con el método correspondiente para Android 5.x
                    this.finishAfterTransition();
                    return true;
                }
                // Dejar que el sistema maneje el comportamiento del up button
                return false;
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public static void launch(Activity context, int position, View sharedView ,List<Book> items) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(EXTRA_POSITION, position);
        itemss=items;
        context.startActivity(intent);
    }
}
