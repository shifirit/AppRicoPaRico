package com.herprogramacion.restaurantericoparico.net;

import android.preference.PreferenceManager;
import android.util.Log;

import com.herprogramacion.restaurantericoparico.ui.MyApp;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class BookClient {
    private static final String API_BASE_URL = "http://www.vam.ac.uk/api/json/museumobject/";
    private AsyncHttpClient client;

    public BookClient() {
        this.client = new AsyncHttpClient();
    }

    private String getApiUrl(String relativeUrl) {
        return API_BASE_URL + relativeUrl;
    }

    // Method for accessing the search API
    public void getBooks(final String query, JsonHttpResponseHandler handler) {
        try {
            String url;

            String year = PreferenceManager.getDefaultSharedPreferences(MyApp.getContext()).getString("edit_text_preference_2","0");
            String yearAfter = "-2000";

            url = getApiUrl("search?before="+year+"&after="+yearAfter+"&q="+URLEncoder.encode(query, "utf-8")+"&images=1");
            
            Log.d("Url",url + URLEncoder.encode(query, "utf-8"));
            client.get(url, handler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    // Method for accessing books API to get publisher and no. of pages in a book.
    public void getExtraBookDetails(String openLibraryId, JsonHttpResponseHandler handler) {
        String url = getApiUrl("museumobject/");
        client.get(url + openLibraryId + ".json", handler);
    }

}